import { configureStore, ThunkAction, AnyAction } from "@reduxjs/toolkit";

import queryStringReducer from "./queryString";
import iconsReducer from "./icons";
import themeReducer from "./theme";

export const storeCreator = () =>
  configureStore({
    reducer: {
      queryString: queryStringReducer,
      icons: iconsReducer,
      theme: themeReducer,
    },
  });

export type AppStore = ReturnType<typeof storeCreator>;
export type RootState = ReturnType<AppStore["getState"]>;
export type AppDispatch = AppStore["dispatch"];
export type AppThunk = ThunkAction<void, RootState, any, AnyAction>;
