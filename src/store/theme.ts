import { createSlice } from "@reduxjs/toolkit";

type Theme = "dark" | "light";

const initialState: Theme = "dark";

const slice = createSlice({
  name: "theme",
  initialState,
  reducers: {
    updateTheme: (state, action) => {
      return action.payload;
    },
  },
});

export default slice.reducer;
export const { updateTheme } = slice.actions;
