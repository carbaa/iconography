import {
  useSelector as originalUseSelector,
  useDispatch as originalUseDispatch,
  TypedUseSelectorHook,
} from "react-redux";
import { AppDispatch, RootState } from "./store";

export const useDispatch = () => originalUseDispatch<AppDispatch>();
export const useSelector: TypedUseSelectorHook<RootState> = originalUseSelector;
