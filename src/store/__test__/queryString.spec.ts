import QueryStringReducer, { update } from "../queryString";

describe("Query string slice", () => {
  it("Query string reducer updates the state as the update action's payload", () => {
    const initialState = "";
    const testQueryString = "test";
    const expectedState = testQueryString;

    const action = update(testQueryString);

    expect(QueryStringReducer(initialState, action)).toEqual(expectedState);
  });
});
