import iconsReducer, { updateIcons } from "../icons";

describe("Icons slice", () => {
  it("Icon reducer replaces icons array with update action's payload", () => {
    const initialState = { icons: [], error: null };
    const testPayload = [
      {
        id: "test1",
        label: "First one",
        membership: {
          free: ["solid"],
        },
      },
      {
        id: "test2",
        label: "Second one",
        membership: {
          free: ["brand"],
        },
      },
    ];
    const testAction = updateIcons(testPayload);
    const expectedState = { icons: testPayload, error: null };

    expect(iconsReducer(initialState, testAction)).toEqual(expectedState);
  });
});
