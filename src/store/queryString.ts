import { createSlice } from "@reduxjs/toolkit";

const slice = createSlice({
  name: "queryString",
  initialState: "",
  reducers: {
    update: (state, action) => {
      return action.payload;
    },
  },
});

export default slice.reducer;
export const { update } = slice.actions;
