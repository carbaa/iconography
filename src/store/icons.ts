import { createSlice } from "@reduxjs/toolkit";
import debounce from "lodash/debounce";

import { search, Icon } from "../client/searchQuery";
import { AppThunk } from "./store";

interface Icons {
  icons: Array<Icon>;
  error: { message: null | string; time: null | number };
}

const initialState: Icons = {
  icons: [],
  error: { message: null, time: null },
};

const slice = createSlice({
  name: "icons",
  initialState,
  reducers: {
    updateIcons: (state, action) => {
      state.icons = action.payload;
    },
    updateIconsError: (state, action) => {
      state.error = action.payload;
    },
  },
});

const searchThunk: AppThunk = (dispatch, getState, query: string) => {
  const version = process.env.FONTAWESOME_VERSION || "6.0.0";
  const limit = Number(process.env.QUERY_LIMIT || 30);
  const observable = search(version, query, limit);

  observable.subscribe({
    next: (data) => {
      const freeIcons = data.search.filter(
        (icons) => icons.membership.free.length > 0
      );
      dispatch(updateIcons(freeIcons));
      dispatch(updateIconsError({ message: null, time: null }));
    },
    error: (error: Error) => {
      dispatch(updateIconsError({ message: error.message, time: Date.now() }));
    },
  });
};

const debouncedSearchThunk = debounce(searchThunk, 500);

const searchThunkCreator = (query: string) => (dispatch, getState) =>
  debouncedSearchThunk(dispatch, getState, query);

export default slice.reducer;
export const { updateIcons, updateIconsError } = slice.actions;
export { searchThunkCreator };
