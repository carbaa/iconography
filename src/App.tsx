import React, { lazy } from "react";

import { storeCreator } from "./store/store";
import { Provider } from "react-redux";
import { useSelector } from "./store/hooks";

import "@fortawesome/fontawesome-free/css/fontawesome.css";
import "@fortawesome/fontawesome-free/css/brands.css";
import "@fortawesome/fontawesome-free/css/solid.css";

const Searchbar = lazy(() => import("./components/Searchbar"));
const IconList = lazy(() => import("./components/IconList"));
const ThemeToggle = lazy(() => import("./components/ThemeToggle"));
const ErrorMessage = lazy(() => import("./components/ErrorMessage"));

const App = () => {
  const theme = useSelector((state) => state.theme);
  const textColor = theme === "dark" ? "text-white" : "text-black";
  const backgroundColor = theme === "dark" ? "bg-gray-700" : "bg-white";

  return (
    <div className={`w-full min-h-screen ${textColor} ${backgroundColor}`}>
      <div
        className={`w-full max-w-screen-lg mx-auto bg-transparent font-sans`}
      >
        <div className="w-full h-16 flex justify-end align-center pr-4">
          <ThemeToggle />
        </div>
        <h1 className="text-center text-4xl">iconography</h1>
        <h2 className="text-center text-lg lowercase mb-4">
          Font Awesome search engine
        </h2>
        <Searchbar />
        <div className="relative">
          <ErrorMessage />
          <IconList />
        </div>
      </div>
    </div>
  );
};

export default function AppWithStore() {
  const store = storeCreator();

  return (
    <Provider store={store}>
      <App />
    </Provider>
  );
}
