import React, { Suspense, lazy } from "react";
import ReactDOM from "react-dom";
import "./index.css";

const App = lazy(() => import("./App"));

ReactDOM.render(
  <Suspense fallback={<div>Wait</div>}>
    <App />
  </Suspense>,
  document.querySelector("#root")
);
