import React, { ChangeEvent, FormEvent } from "react";

import { useDispatch, useSelector } from "../store/hooks";
import { update } from "../store/queryString";
import { searchThunkCreator } from "../store/icons";

export default function Searchbar() {
  const dispatch = useDispatch();
  const queryString = useSelector((state) => state.queryString);
  const theme = useSelector((state) => state.theme);
  const backgroundColor = theme === "dark" ? "bg-gray-800" : "bg-white";

  const onChangeHandler = (e: ChangeEvent<HTMLInputElement>) => {
    const newQueryString = e.target.value;

    dispatch(update(newQueryString));
    dispatch(searchThunkCreator(newQueryString));
  };

  const onSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
  };

  return (
    <form className="flex justify-center content-center" onSubmit={onSubmit}>
      <input
        type="text"
        id="searchbar"
        className={`h-12 w-48 px-2 mb-8 font-sans font-light transition
        ${backgroundColor}
        border border-gray-500 border-opacity-25 rounded-sm
        hover:ring-2 hover:ring-blue-400 hover:ring-opacity-50 
        focus:transition-none focus:ring-8 focus:ring-opacity-50`}
        value={queryString}
        onChange={onChangeHandler}
        placeholder="ex) coffee"
      />
    </form>
  );
}
