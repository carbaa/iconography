import React from "react";
import { render, RenderResult } from "@testing-library/react";
import { Provider } from "react-redux";

import { AppDispatch, AppStore, storeCreator } from "../../store/store";
import { updateIcons } from "../../store/icons";
import IconList from "../IconList";

describe("IconList", () => {
  let context: RenderResult;
  let store: AppStore;
  let dispatch: AppDispatch;

  beforeEach(() => {
    store = storeCreator();
    dispatch = store.dispatch;

    context = render(
      <Provider store={store}>
        <IconList />
      </Provider>
    );
  });

  it("Display icons in the store by listing the label of the icons", () => {
    const testPayload = [
      {
        id: "test1",
        label: "First one",
        membership: {
          free: ["solid"],
        },
      },
      {
        id: "test2",
        label: "Second one",
        membership: {
          free: ["brand"],
        },
      },
    ];

    dispatch(updateIcons(testPayload));

    expect(context.getByText("First one")).not.toBeNull();
    expect(context.getByText("Second one")).not.toBeNull();
  });
});
