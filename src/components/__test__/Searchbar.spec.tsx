import React from "react";
import ReactTestUtils from "react-dom/test-utils";
import { render, RenderResult } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { Provider } from "react-redux";

import { storeCreator } from "../../store/store";
import Searchbar from "../Searchbar";

describe("Searchbar", () => {
  let context: RenderResult;

  beforeEach(() => {
    context = render(
      <Provider store={storeCreator()}>
        <Searchbar />
      </Provider>
    );
  });

  it("sets the value of the textbox to the value of queryString value in the store", () => {
    const textInput = context.getByDisplayValue("");
    userEvent.type(textInput, "test");

    expect(context.getByDisplayValue("test")).not.toBeNull();
  });

  it("doesn't submit form", () => {
    const form = context.container.querySelector("form");
    const mockPreventDefault = jest.fn();
    const mockEvent = {
      preventDefault: mockPreventDefault,
    };

    ReactTestUtils.Simulate.submit(form, mockEvent);
    expect(mockPreventDefault).toHaveBeenCalled();
  });
});
