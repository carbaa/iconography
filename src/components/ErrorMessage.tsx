import React from "react";
import { useSelector } from "../store/hooks";

export default function ErrorMessage() {
  const error = useSelector((state) => state.icons.error);
  const [additionalClasses, setAdditionalClasses] = React.useState("opacity-0");
  let timeoutId: ReturnType<typeof setTimeout> = null;

  React.useEffect(() => {
    if (error.message) {
      setAdditionalClasses("opacity-100");
      timeoutId = setTimeout(() => {
        setAdditionalClasses("opacity-0");
      }, 5000);
    }

    return () => {
      clearTimeout(timeoutId);
    };
  }, [error]);

  return (
    <div
      className={`absolute w-full mt-4 flex justify-center transition-opacity duration-1000 ${additionalClasses}`}
    >
      <p className="bg-red-500 text-black text-sm py-4 px-4 text-ellipsis rounded-lg">
        {error.message}
      </p>
    </div>
  );
}
