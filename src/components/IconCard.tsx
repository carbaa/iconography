import React from "react";

interface IconCardProps {
  id: string;
  label: string;
  iconFamily: "brands" | "solid";
}

const IconCard = ({ id, label, iconFamily }: IconCardProps) => {
  return (
    <div className="w-32 h-32 border flex flex-col justify-center content-center">
      <div className="flex justify-center content-center">
        <i className={`fa-${iconFamily} fa-${id}`} />
      </div>
      <p className="text-sm text-center">{label}</p>
      <p className="text-xs text-center">{`(${id})`}</p>
    </div>
  );
};

export default IconCard;
