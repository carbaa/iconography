import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faLightbulb, faMoon } from "@fortawesome/free-solid-svg-icons";
import { useSelector, useDispatch } from "../store/hooks";
import { updateTheme } from "../store/theme";

export default function ThemeToggle() {
  const dispatch = useDispatch();

  const theme = useSelector((state) => state.theme);
  const icon = theme === "dark" ? faLightbulb : faMoon;
  const onClickHandler = () => {
    dispatch(updateTheme(theme === "dark" ? "light" : "dark"));
  };

  return (
    <button onClick={onClickHandler}>
      <FontAwesomeIcon icon={icon} size="lg" />
    </button>
  );
}
