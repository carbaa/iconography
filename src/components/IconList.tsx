import React from "react";
import { useSelector } from "../store/hooks";
import IconCard from "./IconCard";

const IconList = () => {
  const { icons } = useSelector((state) => state.icons);

  return (
    <section className="grid grid-cols-2 sm:grid-cols-4 gap-4 sm:gap-8 justify-items-center">
      {icons.map(({ id, label, membership }) => {
        const iconFamily = membership.free.includes("brands")
          ? "brands"
          : "solid";

        return (
          <IconCard key={id} id={id} label={label} iconFamily={iconFamily} />
        );
      })}
    </section>
  );
};

export default IconList;
