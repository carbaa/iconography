import { fetchQuery, graphql } from "relay-runtime";
import environment from "./environment";
import { searchQuery } from "./__generated__/searchQuery.graphql";

const SearchQuery = graphql`
  query searchQuery($version: String!, $query: String!, $first: Int) {
    search(version: $version, query: $query, first: $first) {
      id
      label
      membership {
        free
      }
    }
  }
`;

export const search = (version: string, query: string, limit: number) =>
  fetchQuery<searchQuery>(environment, SearchQuery, {
    version,
    query,
    first: limit,
  });

export interface Icon {
  id: string;
  label: string;
  membership: {
    free: string[];
  };
}
