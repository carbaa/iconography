import { Network, Environment, RecordSource, Store } from "relay-runtime";
import { client } from "./client";

export default new Environment({
  network: Network.create(client),
  store: new Store(new RecordSource()),
});
