/**
 * @generated SignedSource<<cf89ab17c3f98dd22800cd6504b3b4f9>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Query } from 'relay-runtime';
export type searchQuery$variables = {
  version: string;
  query: string;
  first?: number | null;
};
export type searchQuery$data = {
  readonly search: ReadonlyArray<{
    readonly id: string;
    readonly label: string;
    readonly membership: {
      readonly free: ReadonlyArray<string>;
    };
  } | null> | null;
};
export type searchQuery = {
  variables: searchQuery$variables;
  response: searchQuery$data;
};

const node: ConcreteRequest = (function(){
var v0 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "first"
},
v1 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "query"
},
v2 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "version"
},
v3 = [
  {
    "alias": null,
    "args": [
      {
        "kind": "Variable",
        "name": "first",
        "variableName": "first"
      },
      {
        "kind": "Variable",
        "name": "query",
        "variableName": "query"
      },
      {
        "kind": "Variable",
        "name": "version",
        "variableName": "version"
      }
    ],
    "concreteType": "Icon",
    "kind": "LinkedField",
    "name": "search",
    "plural": true,
    "selections": [
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "id",
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "label",
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "concreteType": "Membership",
        "kind": "LinkedField",
        "name": "membership",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "free",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": [
      (v0/*: any*/),
      (v1/*: any*/),
      (v2/*: any*/)
    ],
    "kind": "Fragment",
    "metadata": null,
    "name": "searchQuery",
    "selections": (v3/*: any*/),
    "type": "RootQueryType",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": [
      (v2/*: any*/),
      (v1/*: any*/),
      (v0/*: any*/)
    ],
    "kind": "Operation",
    "name": "searchQuery",
    "selections": (v3/*: any*/)
  },
  "params": {
    "cacheID": "b0eb05fd691f7333e050242d0f8ffce6",
    "id": null,
    "metadata": {},
    "name": "searchQuery",
    "operationKind": "query",
    "text": "query searchQuery(\n  $version: String!\n  $query: String!\n  $first: Int\n) {\n  search(version: $version, query: $query, first: $first) {\n    id\n    label\n    membership {\n      free\n    }\n  }\n}\n"
  }
};
})();

(node as any).hash = "936a86266fa5a08ad647b8e085439ee9";

export default node;
