import { RequestParameters, Variables } from "relay-runtime";

export const client = async (
  params: RequestParameters,
  variables: Variables
) => {
  const url = process.env.GRAPHQL_API;

  try {
    const response = await fetch(url, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        query: params.text,
        variables,
      }),
    });

    const json = await response.json();
    if ("errors" in json) throw new Error(JSON.stringify(json.errors));
    return json;
  } catch (error) {
    throw error;
  }
};
