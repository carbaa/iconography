require('dotenv').config()

module.exports = (_, config) => {
  config.env.GRAPHQL_API = process.env.GRAPHQL_API
  return config
}