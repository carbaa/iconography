describe("Listing icons", () => {
  it("Displays a list of icons given a query", () => {
    cy.intercept("POST", Cypress.env("GRAPHQL_API"), {
      fixture: "search.json",
    });

    cy.visit("/");
    cy.get("#searchbar").type("coff");

    cy.contains("Coffee");
    cy.contains("Mug Hot");
  });
});
